(import (chicken io))
(import (chicken process))
(import srfi-13)
(import srfi-69)
(import (chicken process-context))


(define special-cases (make-hash-table))
(hash-table-set! special-cases 'cd (lambda (i) (change-directory (string-join i)) ""))


(define (execute-command quotedExpr) 
  (let ((execString (foldl 
		      (lambda (state i) 
			(string-join (list state 
					   (if (list? i) 
					     (execute-command i) 
					     (symbol->string i))) ))
		      "" quotedExpr)))
    (let (( p (open-input-pipe execString )))
      (read-string #f p)

      )))


(define (exec2-command quotedExpr)
  (let ((args (map (lambda (i) 
		     (if (list? i) 
		       (string-trim-both (exec2-command i))
		       (if (symbol? i) (symbol->string i) i))) (cdr quotedExpr))))
    ((if (hash-table-exists? special-cases (car quotedExpr)) 
       (hash-table-ref special-cases (car quotedExpr)) 
       (lambda (args) (read-string #f (process (symbol->string (car quotedExpr)) args )))
       ) args)
      ))


(define (loop count)  
  (print* count "> ") 
  (let ((val (read-line)))
    (let ((quotedExpr (read (open-input-string val))))

    (print (exec2-command quotedExpr))

    (loop (+ count 1))
    )))

(loop 0)


